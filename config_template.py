#!/usr/bin/env python3

'''
copy this file to ~/.config/launch-in-same-path/config.py
and implement the following functions as desired
'''

def app_to_path(app: str, title: str) -> 'str|None':
	'''
	This function is called first.
	Return the path which shall be opened or None for the default behavior.
	Default behavior is to open a path contained in the title or
	opening the downloads directory if app is a web browser.
	'''
	return None

def web_title_to_path(title: str) -> 'str|None':
	'''
	This function is called if the app is recognized as a web browser.
	Return the path which shall be opened or None for the default behavior.
	Default behavior is to open the downloads directory (as returned by xdg-user-dir).
	'''
	return None

def title_to_path(title: str) -> 'str|None':
	'''
	This function is called if the app has *not* been recognized as a web browser.
	Return the path which shall be opened or None for the default behavior.
	Default behavior is trying to extract a path from the title.
	'''
	return None

def handle_file(path: str) -> 'str|None':
	'''
	This function is called if the extracted path is a file name instead of a directory name.
	Return the path of the directory which shall be opened or None for the default behavior.
	Default behavior is to open the directory in which the file is located.
	'''
	return None
