#!/usr/bin/env python

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

import os
import re
import subprocess
import logging as log
from collections.abc import Sequence

from config import config
from classes import Window


WEBBROWSER = (
	"firefox",
	"firefox-esr",
	"qutebrowser",
)


def window_to_directory(window: 'Window|None', fallback: str) -> str:
	if window is None:
		log.debug("window is none")
		return fallback

	path = window_to_path(window)

	if path is None:
		log.warning("path is None")
		return fallback

	path = os.path.expanduser(path)
	path = os.path.abspath(path)

	path = handle_file(path)

	if not os.path.isdir(path):
		log.warning("no such directory \"%s\"" % path)
		return fallback

	if not os.access(path, os.X_OK):
		log.warning("permission denied \"%s\"" % path)
		return fallback

	log.debug("returned path: %r" % path)
	return path


def window_to_path(window: Window) -> 'str|None':
	log.debug("app: %s" % (window.app,))

	out = config.app_to_path(window.app, window.name)
	if out:
		return out

	if window.app.lower() in WEBBROWSER:
		out = config.web_title_to_path(window.name)
		if out:
			return out
		return get_download_directory()

	title = window.name

	out = config.title_to_path(title)
	if out:
		return out

	ALACRITTY_SUFFIX = u"�"
	if title[-len(ALACRITTY_SUFFIX):] == ALACRITTY_SUFFIX:
		title = title[:-len(ALACRITTY_SUFFIX)]

	# vim (see vim :h title)
	m = re.match(r"(?P<name>.*?) (?P<mode>[=]?[-+]?)[ ]?\((?P<path>[~/].*)\) - VIM$", title)
	if m:
		path = m.group('path')
		name = m.group('name')
		if name == "COMMIT_EDITMSG":
			path = get_git_work_tree(path)
		if name == "TAG_EDITMSG":
			path = get_git_work_tree(path)
		return path

	m = re.match(r".*?(?P<path>[~/].*)$", title)
	if m:
		path = m.group('path')
		path = os.path.expanduser(path)
		if os.path.exists(path):
			return path

		# idle
		path = re.sub(r"\s*[(][^()]+[)]$", "", path)
		if os.path.exists(path):
			return path

	# imv
	m = re.match(r".*?(\[.*?\]\s*)*(?P<path>[~/].*?)\s*(\[[^[\]]*\]\s*)*$", title)
	if m:
		path = m.group('path')
		path = os.path.expanduser(path)
		if os.path.exists(path):
			return path

	m = re.match(r".*?(?P<path>[~/].*?)[*]?( - .*)?$", title)
	if m:
		path = m.group('path')
		path = os.path.expanduser(path)
		if os.path.exists(path):
			return path

		# PyCharm:
		if path[-1:] == "]":
			path = path[:-1]
			return path

	if title == "untitled - TeXstudio":
		return os.path.expanduser("/tmp")

	log.warning("failed to parse title %r" % title)
	return None


def get_git_work_tree(path: str) -> str:
	'''
	input: a path inside of a git directory where commit messages and stuff are saved
	output: the corresponding top level directory
	'''

	path = os.path.expanduser(path)
	path = os.path.abspath(path)
	cmd = ['git', 'rev-parse', '--show-toplevel']
	p = subprocess.run(cmd, cwd=path, encoding='utf-8', capture_output=True, text=True)
	if p.returncode == 0:
		return p.stdout.rstrip('\n')

	path = os.path.split(path)[0]
	return path


def handle_file(path: str) -> str:
	if not os.path.isfile(path):
		return path

	out = config.handle_file(path)
	if out:
		return out

	path = os.path.split(path)[0]
	return path



def run_get(cmd: 'Sequence[str]') -> 'str|None':
	try:
		out = subprocess.check_output(cmd, text=True)
		out = out.rstrip("\n")
		return out

	except (subprocess.SubprocessError, OSError):
		return None


def get_home() -> str:
	return os.path.expanduser("~")

def get_download_directory() -> 'str|None':
	cmd = ('xdg-user-dir', 'DOWNLOAD')
	path = run_get(cmd)
	return path


def get_git_root(path: str) -> 'str|None':
	os.chdir(path)

	cmd = ["git", "rev-parse", "--show-toplevel"]
	out = run_get(cmd)

	return out

def get_latex_root(path: str) -> 'str|None':
	last = path
	while "main.tex" not in os.listdir(path):
		path = os.path.split(path)[0]
		if path == last:
			return None

		last = path

	return path



if __name__ == '__main__':
	import ipc_api
	print(window_to_directory(ipc_api.get_current_window(), fallback='<error: no current window>'))
