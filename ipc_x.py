#!/usr/bin/env python3

import subprocess
import re
import shutil

from classes import Window

def get_current_window() -> 'Window|None':
	window_id = subprocess.check_output(['xdotool', 'getwindowfocus'], text=True).strip()
	wm_title = subprocess.check_output(['xprop', '-id', window_id, '_NET_WM_NAME'], text=True).strip()
	if not wm_title.endswith('"'):
		wm_title = subprocess.check_output(['xprop', '-id', window_id, 'WM_NAME'], text=True).strip()
	wm_class = subprocess.check_output(['xprop', '-id', window_id, 'WM_CLASS'], text=True).strip()

	reo_val = re.compile('"(.*?)"')

	wm_title_match = reo_val.search(wm_title)
	if wm_title_match is None:
		return None
	wm_title = wm_title_match.group(1)

	wm_class_match = reo_val.search(wm_class)
	if wm_class_match is None:
		return None
	wm_class = wm_class_match.group(1)

	return Window(wm_class, wm_title)

def is_supported() -> bool:
	if not shutil.which('xdotool'):
		return False
	if not shutil.which('xprop'):
		return False
	return True


if __name__ == '__main__':
	if is_supported():
		print(get_current_window())
	else:
		print('xdotool or xprop is/are missing')
