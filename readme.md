# Open a program in the same working directory

Output of `./main.py --help`:

	usage:
		main.py [-d PATH] COMMAND ARGUMENTS
		main.py [-d PATH] [-l] --term TERMINAL -- CLIPROGRAM ARGUMENTS

	This program tries to extract the current working directory
	from the title of the current window and executes the given command.
	Each occurrence of the wildcard {cwd} in the arguments is replaced by the determined path.
	Supported window managers:
		- i3
		- sway
		- all other X server based window managers if xdotool and xprop are installed

	If you want to open a program with a command line interface
	you need to open a terminal which runs a shell where you first
	change the directory and then run the desired command.
	Dealing with so many nested layers of quoting not only in shell
	commands but in an i3 config file on top can become confusing.
	Therefore this program offers a special flag for that:
	Everything between --term and -- is treated as a terminal command
	to be executed directly. Everything after the -- is put in a shell
	command which is then appended to the terminal command.
	This program takes care of changing the directory and quoting things.
	For example if you are currently in the directory /tmp
	and launch-in-same-path refers to this file then
		$ launch-in-same-path --term sakura -x -- ranger
	will basically execute:
		$ sakura -x 'bash -c "cd /tmp; ranger"'
	(but with the path quoted so that it may contain spaces).

	Some terminals do not expect the command as a single string but as a
	list of strings, in that case use the --cmd-as-list flag:
		$ launch-in-same-path --cmd-as-list --term kitty -e -- ranger
	will basically execute:
		$ kitty -e sh -c 'cd /tmp; ranger'
	(but with the path quoted so that it may contain spaces).

	Please note that the optional arguments must be given in the order
	in which they are listed below.

	optional arguments:
	  -h, --help            show this help message and exit
	  -d, --fallback PATH   directory to be used if extraction from window title fails
				(defaults to the home directory)
	  -p, --path PATH       use this directory regardless of open windows
				(in case you only care about --term and not the extraction)
	  -l, --cmd-as-list     only in combination with --term, see above
	  --term TERMINAL       the command shall be run in a shell in a terminal, see above

	examples:
		main.py termite -d "{cwd}"
		main.py --term termite -e -- ranger
		main.py --cmd-as-list --term kitty -e -- vim


## Configuration

You can easily extend this program with custom python functions to parse the title of a window.
Just copy [config_template.py](https://gitlab.com/erzo/launch-in-same-path/-/blob/master/config_template.py?ref_type=heads) to ~/.config/launch-in-same-path/config.py and implement the desired function as described in the template.

For debugging you can use the standard library [logging](https://docs.python.org/3/library/logging.html#module-level-functions).
The output of `logging.error("message")` and `logging.warning("message")` is written to a file called log_error.txt located next to main.py.
I am printing the content of this file when opening a new terminal with the following line in my bashrc:

```bash
[ -f ~/.local/utils/launch-in-same-path/log_error.txt ] && cat ~/.local/utils/launch-in-same-path/log_error.txt
```


## Installation

This program uses standard library only, so there is no need for a virtual environment.
Just clone this repo to ~/.local/utils or /opt or wherever you like to keep manually installed software.
Or you can add this as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to your [dotfiles repo](https://wiki.archlinux.org/title/Dotfiles#Tracking_dotfiles_directly_with_Git).
Then use the absolute path to main.py in your window manager config.

Updates are as easy as a `git pull`.


## i3/sway config

I am using this program in the following way in my i3/sway config file:

	bindsym $mod+Return        exec $cwd $term_in "{cwd}"
	bindsym $mod+Shift+Return  exec $cwd $term_with "ranger"
	bindsym $mod+period        exec $cwd $term_with "vim"
	bindsym $mod+y             exec $cwd $term_with "bpython3"

where I have defined `$cwd` as the path to main.py
and `$term_in` and `$term_with`
as the following on Arch:

	set $term_in   termite -d
	set $term_with --term termite -e --

and the following on Debian:

	set $term_in   --cmd-as-list kitty -d
	set $term_with --cmd-as-list --term kitty -e --

For more information on using variables in the i3/sway config see the [i3 documentation](https://i3wm.org/docs/userguide.html#variables).
Please note that "Variables expansion is not recursive
so it is not possible to define a variable with a value containing another variable.
There is no fancy handling and there are absolutely no plans to change this."


## vim config

Vim (vi improved) is the editor of my choice.
In order to make sure that it actually shows the unabbreviated path of the current file in the window title
I have the following in `~/.vimrc`:

	set title
	set titlelen=0


## ranger config

Ranger is a file manager.
In order to make sure that it actually shows the unabbreviated path of the current file in the window title
I have the following in `~/.config/ranger/rc.conf`:

	set update_title true
	set shorten_title 0

Please note that this does not work with all terminals.
Terminals where I know that ranger is capable to set the window title:

- termite
- kitty

Terminals where I know that ranger is *not* capable to set the window title:

- gnome-terminal
- sakura


## GIMP config

[GIMP](https://www.gimp.org/) is the GNU Image Manipulation Program.
Configure GIMP to show the entire file path in
Edit > Preferences > Image Windows > Title & Status:

```
%D*%F [%p.%i] (%t, %o, %L) %wx%h
```

The wildcards are explained in the
[GIMP documentation](https://testing.docs.gimp.org/3.0/en/gimp-prefs-image-window-title.html).

Then add the following to your ~/.config/launch-in-same-path/config.py:

```python
import re

def title_to_path(title: str) -> 'str|None':
	if "GIMP" in title:
		m = re.search(r' \[[0-9+\.[0-9]+\] ', title)
		assert m is not None, "Invalid regex for path separator in GIMP title"
		path = title[:m.start()]
		if path.startswith("*"):
			path = path[1:]
		return path

	return None
```


# License

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the copying.txt file for more details.


[i3]: https://i3wm.org/
[sway]: https://swaywm.org/
