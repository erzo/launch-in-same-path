#!/usr/bin/env python3

import ipc_x
if ipc_x.is_supported():
	get_current_window = ipc_x.get_current_window
else:
	import ipc_i3_sway
	get_current_window = ipc_i3_sway.get_current_window

if __name__ == '__main__':
	print(get_current_window())
