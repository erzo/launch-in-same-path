#!/usr/bin/env python3

class Window:
	
	def __init__(self, app: 'str|None', name: 'str|None') -> None:
		if app is None:
			app = ''
		if name is None:
			name = ''
		self.app = app
		self.name = name

	def __str__(self) -> str:
		return "\n".join([
			"app:  %s" % self.app,
			"name: %s" % self.name,
		])
