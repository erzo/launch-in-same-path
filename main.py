#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

"""
usage:
	main.py [--venv [--install]] [-d PATH] COMMAND ARGUMENTS
	main.py [--venv [--install]] [-d PATH] [-l] --term TERMINAL -- CLIPROGRAM ARGUMENTS

This program tries to extract the current working directory
from the title of the current window and executes the given command.
Each occurrence of the wildcard {cwd} in the arguments is replaced by the determined path.
Supported window managers:
	- i3
	- sway
	- all other X server based window managers if xdotool and xprop are installed

If you want to open a program with a command line interface
you need to open a terminal which runs a shell where you first
change the directory and then run the desired command.
Dealing with so many nested layers of quoting not only in shell
commands but in an i3 config file on top can become confusing.
Therefore this program offers a special flag for that:
Everything between --term and -- is treated as a terminal command
to be executed directly. Everything after the -- is put in a shell
command which is then appended to the terminal command.
This program takes care of changing the directory and quoting things.
For example if you are currently in the directory /tmp
and launch-in-same-path refers to this file then
	$ launch-in-same-path --term sakura -x -- ranger
will basically execute:
	$ sakura -x 'bash -c "cd /tmp; ranger"'
(but with the path quoted so that it may contain spaces).

Some terminals do not expect the command as a single string but as a
list of strings, in that case use the --cmd-as-list flag:
	$ launch-in-same-path --cmd-as-list --term kitty -e -- ranger
will basically execute:
	$ kitty -e sh -c 'cd /tmp; ranger'
(but with the path quoted so that it may contain spaces).

Please note that the optional arguments must be given in the order
in which they are listed below.

optional arguments:
  -h, --help            show this help message and exit
  --venv                try to find a python virtual environment
                        and run COMMAND in that environment
                        by default it is assumed to be called venv
                        but a different name can be given with a =
                        e.g. --venv=.tox/py3
  --install             (only in combination with --venv)
                        install command if it is not installed
  -d, --fallback PATH   directory to be used if extraction from window title fails
                        (defaults to the home directory)
  -p, --path PATH       use this directory regardless of open windows
                        (in case you only care about --term and not the extraction)
  -l, --cmd-as-list     only in combination with --term, see above
  --term TERMINAL       the command shall be run in a shell in a terminal, see above

examples:
	main.py termite -d "{cwd}"
	main.py --term termite -e -- ranger
	main.py --cmd-as-list --term kitty -e -- vim
"""

import sys
import subprocess
import os
import shlex
import logging as log

import ipc_api
import parser

WILDCARD = "{cwd}"
cmd_as_list = False


def argparse(args: 'list[str]') -> 'tuple[str|None, bool, str|None, str, list[str]]':
	global cmd_as_list

	i = 0
	n = len(args)

	if "-h" in args or "--help" in args:
		print(__doc__.strip())
		exit(0)

	venv = None
	install = False
	if n > i+1 and args[i].startswith("--venv"):
		if "=" in args[i]:
			venv = args[i].split("=", 1)[1]
		else:
			venv = "venv"

		i += 1
		if args[i] == "--install":
			install = True
			i += 1

	path = None
	if n > i+2 and args[i] in ["-d", "--fallback"]:
		i += 1
		fallback = args[i]
		i += 1
	elif n > i+2 and args[i] in ["-p", "--path"]:
		i += 1
		path = args[i]
		fallback = ''
		i += 1
	else:
		fallback = parser.get_home()

	if n > i+1 and args[i] in ["-l", "--cmd-as-list"]:
		i += 1
		cmd_as_list = True

	cmd = args[i:]
	
	return venv, install, path, fallback, cmd

def replace_wildcard(cmd: 'list[str]', path: str) -> 'list[str]':
	if cmd[0] == "--term":
		i = cmd.index("--", 1)
		new_cmd = cmd[1:i]
		main = " ".join(cmd[i+1:])
		path = shlex.quote(path)
		cd = "cd %s; %s" % (path, main)
		if cmd_as_list:
			new_cmd.append("sh")
			new_cmd.append("-c")
			new_cmd.append(cd)
		else:
			cd = shlex.quote(cd)
			sh =  "sh -c " +  cd
			new_cmd.append(sh)
	else:
		new_cmd = [w.replace(WILDCARD, path) for w in cmd]

	if cmd == new_cmd:
		new_cmd.append(path)

	return new_cmd

def replace_in_command(cmd: 'list[str]', old: str, new: str) -> 'list[str]':
	return [w.replace(old, new) for w in cmd]

def find_venv(path: str, venv: str) -> 'str|None':
	'''
	:param path: The directory in which the command is supposed to be executed
	:param venv: The name of the virtual environment
	:return: The path to the bin directory in the virtual environment or None if no virtual environment is found
	'''
	path = os.path.realpath(path)
	while True:
		if os.path.isdir(os.path.join(path, venv)):
			return os.path.join(path, venv, 'bin')

		last_path = path
		path = os.path.dirname(path)
		if path == last_path:
			return None

def get_program_index(cmd: 'list[str]') -> int:
	if cmd[0] == "--term":
		i = cmd.index("--", 1)
		return i + 1
	return 0

def main() -> None:
	args = sys.argv[1:]
	venv, install, path, fallback, cmd = argparse(args)

	if not path:
		try:
			window = ipc_api.get_current_window()
			path = parser.window_to_directory(window, fallback)
		except Exception as excp:
			log.error("Exception while trying to figure out the current working directory:", exc_info=excp)
			path = fallback

	if venv:
		venv_bin = find_venv(path, venv)
		if venv_bin:
			i = get_program_index(cmd)
			program = os.path.join(venv_bin, cmd[i])
			if install and not os.path.exists(program):
				pip = os.path.join(venv_bin, 'pip')
				package = cmd[i]
				p = subprocess.run([pip, 'install', package], stderr=subprocess.PIPE, check=False, text=True)
				stderr = p.stderr
				if p.returncode != 0:
					log.error("Error while trying to install %r" % package)
					log.error(stderr)
			if os.path.exists(program):
				cmd[i] = program
			else:
				log.warning("%r does not exist, falling back to global" % program)
		else:
			log.info("no venv found")

	cmd = replace_in_command(cmd, r'\x3b', ';')
	cmd = replace_wildcard(cmd, path)
	log.debug("executing %s" % shlex.join(cmd))
	subprocess.Popen(cmd)


# ------- logging -------

def new_file_handler(filename: str, mode: str = "wt", encoding: str = "utf8", fmt: 'str|None' = None, level: 'int|str|None' = None) -> log.FileHandler:
	handler = log.FileHandler(filename, mode=mode, encoding=encoding)
	if fmt is not None:
		formatter = log.Formatter(fmt)
		handler.setFormatter(formatter)
	if level is not None:
		handler.setLevel(level)
	return handler

def logging_setup() -> None:
	path = os.path.split(os.path.abspath(__file__))[0]

	#log.basicConfig(
	#	filename = os.path.join(path, "log_debug.txt"),
	#	filemode = "wt",
	#	level    = log.DEBUG,
	#	format   = "[%(levelname)-8s] %(message)s",
	#)

	logger = log.getLogger()

	#logger.addHandler(log.StreamHandler())

	logger.addHandler(new_file_handler(
		filename = os.path.join(path, "log_error.txt"),
		level    = log.INFO,
		#form     = "%(levelname)s: %(message)s",
	))


if __name__ == '__main__':
	logging_setup()
	main()
