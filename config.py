#!/usr/bin/env python3

"""
The config file is a python script which may define several functions.
See config_template.py
"""

import os
import logging

APP_NAME = 'launch-in-same-path'
CONFIG_FILE_NAME = 'config.py'

class Config:

	def __init__(self, module: object) -> None:
		self._module = module

	def _call(self, func_name: str, *args: object) -> 'str|None':
		func = getattr(self._module, func_name, None)
		if func:
			try:
				out = func(*args)
			except Exception as e:
				logging.error("an exception has occured while trying to call function %s in config file %r", func_name, self.get_file_name(), exc_info=e)
				return None

			if not (out is None or isinstance(out, str)):
				arg_str = ', '.join(repr(v) for v in args)
				logging.error("calling `%s(%s)` in config file %r has returned invalid value %r, expected str or None", func_name, arg_str, self.get_file_name(), out)
				return None

			return out

		return None

	def get_file_name(self) -> str:
		return self._module.__file__  # type: ignore [attr-defined, no-any-return]


	# functions which can be defined in config file:

	def app_to_path(self, app: str, title: str) -> 'str|None':
		return self._call('app_to_path', app, title)

	def web_title_to_path(self, title: str) -> 'str|None':
		return self._call('web_title_to_path', title)

	def title_to_path(self, title: str) -> 'str|None':
		return self._call('title_to_path', title)

	def handle_file(self, path: str) -> 'str|None':
		return self._call('handle_file', path)


def get_config_file_name() -> str:
	path = os.environ.get('XDG_CONFIG_HOME', None)
	if not path:
		path = os.path.expanduser('~')
		path = os.path.join(path, '.config')
	path = os.path.join(path, APP_NAME)
	fn = os.path.join(path, CONFIG_FILE_NAME)
	return fn

def import_module(path: str) -> object:
	#https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path/67692#67692
	import importlib.util
	name = path
	spec = importlib.util.spec_from_file_location(name, path)
	assert spec is not None
	assert spec.loader is not None
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	return module

def get_config() -> Config:
	fn = get_config_file_name()
	if os.path.isfile(fn):
		try:
			return Config(import_module(fn))
		except Exception as e:
			logging.error("error while trying to load config file %r:" % fn)
			logging.error(e)
	return Config(None)


config = get_config()


if __name__ == '__main__':
	print(config.get_file_name())
