#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

import subprocess
import json
import typing

from classes import Window

T = typing.TypeVar("T")

def join_iter(*l: 'typing.Iterable[T]') -> 'typing.Iterator[T]':
	for i in l:
		yield from i

# ------- check backend -------

I3_MSG   = "i3-msg"
SWAY_MSG = "swaymsg"

def check_backend() -> str:
	cmd = [SWAY_MSG, '--version']
	try:
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		return SWAY_MSG

	except FileNotFoundError:
		return I3_MSG

CMD = check_backend()


# ------- inter process communication -------

def _ipc(t: str) -> typing.Any:
	cmd = [CMD, "-t", t]
	p = subprocess.run(cmd, stdout=subprocess.PIPE)
	return json.loads(p.stdout)

def get_tree() -> typing.Any:
	return _ipc("get_tree")


# ------- get focused node -------

def get_focused_node() -> typing.Any:
	"""
	Please note that the returned container does not need to be a window.
	It might contain several windows (after Super+a).
	Returns the workspace if there are no open windows.
	"""

	root = get_tree()
	assert root['type'] == 'root'

	for child in root['nodes']:
		assert child['type'] == 'output'

		if child['name'] == '__i3':
			# This is a virtual output
			# containing the scratchpad.
			# The scratchpad is special
			# in that it does not have
			# a nodes member, only
			# floating_nodes.
			# Windows on the scratchpad
			# are invisible and cannot
			# be focused.
			continue

		out = _get_focused_node(child)
		if out is not None:
			return out

	return None

def _get_focused_node(node: typing.Any) -> typing.Any:
	for child in join_iter(node['floating_nodes'], node['nodes']):
		if child['focused'] == True:
			return child

		out = _get_focused_node(child)
		if out is not None:
			return out
	
	return None


# ------- get current window -------

def get_current_window() -> 'Window|None':
	w = get_current_window_dict()

	if w['type'] == 'workspace':
		return None

	name = w['name']
	if CMD == SWAY_MSG:
		app = w.get('app_id') or w.get('window_properties', {}).get('class') or w.get('window_properties', {}).get('instance')
	else:
		app = w['window_properties']['class']
	
	return Window(app=app, name=name)

def get_current_window_dict() -> typing.Any:
	node = get_focused_node()

	if node['nodes']:
		return _get_child(node)
	else:
		return node

def _get_child(node: typing.Any) -> typing.Any:
	"""
	I am returning the window with the highest focus-value
	hoping that this is the most recently focussed window.
	Unfortunately I have not found any documentation for
	this value so I don't really know what it means.
	Recent experiments have shown, however, that this value
	does not appear to be that intuitive.
	"""
	m = max(node['focus'])
	i = node['focus'].index(m)

	node = node['nodes'][i]

	if node['nodes']:
		return _get_child(node)
	else:
		return node


# ------- test code -------

def print_tree() -> None:
	root = get_tree()
	assert root['type'] == 'root'

	for out in root['nodes']:
		print("{out[name]} {out[focus]}".format(out=out))
		assert out['type'] == 'output'

		if out["name"] == "__i3":
			continue

		for ws in out['nodes']:
			print("    {ws[name]}: {ws[focus]}".format(ws=ws))
			assert ws['type'] == 'workspace'

			#print(ws)
			for w in ws['nodes']:
				print("         {w[type]} {w[name]}: {w[focused]}".format(w=w))

			for w in ws['floating_nodes']:
				print("         [floating] {w[type]} {w[name]}: {w[focused]}".format(w=w))

		break


if __name__ == '__main__':

	#print_tree()

	print("current window:")
	print(get_current_window())
